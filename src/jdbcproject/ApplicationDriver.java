package jdbcproject;

import java.util.HashMap;
import java.util.Scanner;

public class ApplicationDriver {
  enum searchObject {
    USER, BOOK;
  }

  public static void main(String[] args) throws InterruptedException {

    Scanner scanner = new Scanner(System.in);
    SqlManager queryObject = new SqlManager();
    SqlDisplay displayObject = new SqlDisplay();
    int userChoice;
    HashMap<Integer, Run> Map = new HashMap<>();

    Map.put(1, () -> {
      SqlFunctions.clearConsole();
      SqlFunctions.addNewUser(queryObject);
    });

    Map.put(2, () -> {
      SqlFunctions.clearConsole();
      displayObject.displayResult(queryObject.runDisplayQuery("select * from student"));
    });

    Map.put(3, () -> {
      SqlFunctions.clearConsole();
      SqlFunctions.searchForObject(searchObject.USER, queryObject, displayObject);
    });

    Map.put(4, () -> {
      SqlFunctions.clearConsole();
      System.out.print("Enter the name of the book\t:");
      String bookName = scanner.nextLine();
      System.out.println("Updated values-\t" + queryObject.runUpdateQuery(
          "insert into book(name,book_issued) values ( '" + bookName + "' ," + " 'no' )"));
    });

    Map.put(5, () -> {
      SqlFunctions.clearConsole();
      displayObject.displayResult(queryObject.runDisplayQuery("select * from book"));
    });

    Map.put(6, () -> {
      SqlFunctions.clearConsole();
      SqlFunctions.searchForObject(searchObject.BOOK, queryObject, displayObject);
    });

    Map.put(7, () -> {
      SqlFunctions.clearConsole();
      SqlFunctions.issueBook(queryObject);
    });

    Map.put(8, () -> {
      SqlFunctions.clearConsole();
      SqlFunctions.returnBook(queryObject);
    });

    Map.put(9, () -> {
      SqlFunctions.clearConsole();
      SqlFunctions.showIssues(queryObject, displayObject);
    });

    Map.put(10, () -> {
      SqlFunctions.clearConsole();
      System.out.println("Deleting data");
      queryObject.runUpdateQuery("SET FOREIGN_KEY_CHECKS = 0");
      queryObject.runUpdateQuery("truncate table student");
      queryObject.runUpdateQuery("truncate table book");
      queryObject.runUpdateQuery("truncate table issue");
      queryObject.runUpdateQuery("SET FOREIGN_KEY_CHECKS = 1");
    });

    Map.put(11, () -> {
      SqlFunctions.clearConsole();
      System.out.println("Creating user report");
      displayObject.generateReport(queryObject.runDisplayQuery("select * from student"),
          "student.txt");
      System.out.println("Creating books report");
      displayObject.generateReport(queryObject.runDisplayQuery("select * from book"), "book.txt");
      System.out.println("Creating issue report");
      displayObject.generateReport(queryObject.runDisplayQuery("select * from issue"), "issue.txt");
    });

    Map.put(0, () -> {
      System.out.println("Exiting libray management software\n");
      queryObject.closeConnection();
      scanner.close();
      System.exit(0);
    });

    System.out.println("\t\t\t\t\t\t\t\tLibrary Management System");
    while (true) {
      System.out.println("Enter your choice\n\n");
      System.out.printf("->(0)%-30s\n", "Exit");// done
      System.out.printf("->(1)%-30s\n", "Add a new Library User");// done
      System.out.printf("->(2)%-30s\n", "Show all Library users");// done
      System.out.printf("->(3)%-30s\n", "Search for users"); // done
      System.out.printf("->(4)%-30s\n", "Add a new Book");// done
      System.out.printf("->(5)%-30s\n", "Show all books"); // done
      System.out.printf("->(6)%-30s\n", "Search for book"); // done
      System.out.printf("->(7)%-30s\n", "Issue a new Book"); // done
      System.out.printf("->(8)%-30s\n", "Return a Book");// not done
      System.out.printf("->(9)%-30s\n", "Show all issues"); // done
      System.out.printf("->(10)%-30s\n", "Reset all data"); // done
      System.out.printf("->(11)%-30s\n", "Create Reports");// done
      userChoice = scanner.nextInt();
      scanner.nextLine();
      SqlFunctions.clearConsole();
      if(Map.containsKey(userChoice))
      {
        Map.get(userChoice).runner();
      }
      else
      {
        System.out.println("Wrong option entered");
      }
      Thread.sleep(2000);
      SqlFunctions.clearConsole();
    }
  }
}
