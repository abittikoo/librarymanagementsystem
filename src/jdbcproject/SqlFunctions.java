package jdbcproject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Scanner;
import jdbcproject.ApplicationDriver.searchObject;

public class SqlFunctions {
  static Scanner scanner = new Scanner(System.in);

  // inserting spaces in the console
  public static void clearConsole() {
    for (int LineIterator = 1; LineIterator <= 20; LineIterator++)
      System.out.println();
  }

  // add a new user to the database
  public static void addNewUser(SqlManager queryObject) {
    System.out.print("Enter a new User\t:");
    String UserName[] = scanner.nextLine().trim().split("\\s+");
    if (UserName.length == 0) {
      System.out.println("Name not entered");
    }
    if (UserName.length == 1) {
      System.out.println("Updated Rows-" + queryObject
          .runUpdateQuery("insert into student(first_name) values ( '" + UserName[0] + "' )"));
    } else {
      System.out.println("Updated Rows-"
          + queryObject.runUpdateQuery("insert into student(first_name,last_name) values ( '"
              + UserName[0] + "', " + "'" + UserName[1] + "'" + ")"));
    }
  }

  // search for an object either a book or an user
  public static void searchForObject(searchObject objectChoice, SqlManager queryObject,
      SqlDisplay displayObject) {
    int searchChoice;
    System.out.print("Enter your search choice 1-by id 2-name\t:");
    searchChoice = scanner.nextInt();
    scanner.nextLine();
    if (searchChoice == 1 && objectChoice == searchObject.USER) {
      int userId;
      System.out.print("Enter the user id\t:");
      userId = scanner.nextInt();
      scanner.nextLine();
      displayObject.displayResult(
          queryObject.runDisplayQuery("select * from student where student_id = " + userId));
    }
    if (searchChoice == 2 && objectChoice == searchObject.USER) {
      String username;
      System.out.printf("Enter the first name\t:");
      username = scanner.nextLine();
      displayObject.displayResult(queryObject
          .runDisplayQuery("select * from student where first_name like '%" + username + "%'"));
    }
    if (searchChoice == 1 && objectChoice == searchObject.BOOK) {
      System.out.println("Enter Book Id");
      int bookId = scanner.nextInt();
      System.out.print("\n\n");
      displayObject.displayResult(
          queryObject.runDisplayQuery("select * from book where book_id" + "=" + bookId));
      scanner.nextLine();

    }
    if (searchChoice == 2 && objectChoice == searchObject.BOOK) {
      System.out.println("Enter Book name\n");
      String bookName = scanner.nextLine();
      System.out.print("\n\n");
      displayObject.displayResult(
          queryObject.runDisplayQuery("select * from book where name like '%" + bookName + "%'"));
    }
  }

  // show all book issues
  public static void showIssues(SqlManager queryObject, SqlDisplay displayObject) {
    int searchChoice;
    System.out.println("Enter choice 1-by user 2-by book 3-date 4-all");
    searchChoice = scanner.nextInt();
    scanner.nextLine();
    if (searchChoice == 1) {
      System.out.println("Enter the user id");
      int userId = scanner.nextInt();
      scanner.nextLine();
      displayObject.displayResult(queryObject.runDisplayQuery(
          "select * from issue where student_id=" + userId + " order by issue_date desc"));
    } else if (searchChoice == 2) {
      System.out.println("Enter the book id ");
      int bookId = scanner.nextInt();
      scanner.nextLine();
      displayObject.displayResult(queryObject.runDisplayQuery(
          "select * from issue where book_id=" + bookId + " order by issue_date desc"));
    } else if (searchChoice == 3) {
      System.out.print("Enter start date\tyyyy-mm-dd\t:");
      String startDate = scanner.nextLine();
      System.out.print("\nEnter end date\tyyyy-mm-dd\t:");
      String endDate = scanner.nextLine();
      displayObject.displayResult(
          queryObject.runDisplayQuery("select * from issue where issue_date between '" + startDate
              + "' and '" + endDate + "'"));

    } else if (searchChoice == 4) {
      displayObject.displayResult(
          queryObject.runDisplayQuery("select * from issue order by issue_date desc"));
    }
  }

  // issue a new book
  public static void issueBook(SqlManager queryObject) {
    Timestamp currentTime = new Timestamp(System.currentTimeMillis());
    System.out.print("Enter the user id\t:");
    int userId = scanner.nextInt();
    scanner.nextLine();
    System.out.print("\nEnter the book id\t:");
    int bookId = scanner.nextInt();
    scanner.nextLine();
    ResultSet tempResultBook =
        queryObject.runDisplayQuery("select * from book where book_id=" + bookId);
    ResultSet tempResultUser =
        queryObject.runDisplayQuery("select * from student where student_id=" + userId);
    try {
      if (!tempResultBook.next()) {
        System.out.println("Book does not exsist");
      }
      if (!tempResultUser.next()) {
        System.out.println("User does not exsist");
      }
      if (tempResultBook.getString("book_issued").equals("yes")) {
        System.out.println("Book already issued");
      } else {
        System.out.println("\nRecords updated " + queryObject
            .runUpdateQuery("insert into issue(student_id,book_id,issue_date) values (" + userId
                + "," + bookId + "," + "'" + currentTime.toString().substring(0, 19) + "')"));
        queryObject.runUpdateQuery("update book set book_issued=\"yes\" where book_id=" + bookId);
        queryObject.runUpdateQuery(
            "update student set num_issues=num_issues+1 where student_id=" + userId);
      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  public static void returnBook(SqlManager queryObject) {
    Timestamp currentTime = new Timestamp(System.currentTimeMillis());
    System.out.print("Enter book to be returned\t:");
    int bookId = scanner.nextInt();
    scanner.nextLine();
    ResultSet tempResult =
        queryObject.runDisplayQuery("select * from book where book_id =" + bookId);
    try {
      if (!tempResult.next()) {
        System.out.println("Book does not exsist");
      }

      if (tempResult.getString("book_id").equals("no")) {
        System.out.println("Book not issued yet");
      } else {


        queryObject.runUpdateQuery("update book set book_issued = 'no' where book_id = " + bookId);
        ResultSet tempresult = queryObject.runDisplayQuery(
            "select student_id from issue where return_date is null and book_id = " + bookId);
        tempresult.next();
        int studentId = Integer.parseInt(tempresult.getString("student_id"));
        queryObject.runUpdateQuery(
            "update student set num_issues = num_issues-1 where student_id = " + studentId);
        queryObject.runUpdateQuery("update issue set return_date = '"
            + currentTime.toString().substring(0, 19) + "' where student_id = " + studentId
            + " and book_id = " + bookId + " and return_date is null ");


      }
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

  }
}
