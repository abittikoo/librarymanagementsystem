package jdbcproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlManager {
  private Connection sqlConnection;
  private String databaseName;
  private ResultSet result;
  private String databaseQuery;

  SqlManager() {
    databaseName = "library_database";
    try {
      sqlConnection = DriverManager.getConnection(
          "jdbc:mysql://localhost:3306/" + databaseName + "?useSSL=false", "root", "root");
    } catch (SQLException e) {
      System.out.println("Connection to database failed\n");
    }
  }

  private void createQuery(String Query) {
    databaseQuery = Query;
  }

  public ResultSet runDisplayQuery(String Query) {
    // use single try catch
    Statement NewStatement = null;
    createQuery(Query);
    try {
      NewStatement = sqlConnection.createStatement();
      result = NewStatement.executeQuery(databaseQuery);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
    return result;
  }

  public int runUpdateQuery(String updataQuery) {
    Statement NewStatement = null;
    createQuery(updataQuery);
    int ChangedRows = 0;
    try {
      NewStatement = sqlConnection.createStatement();
      ChangedRows = NewStatement.executeUpdate(databaseQuery);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
    return ChangedRows;

  }

  public void closeConnection() {
    try {
      sqlConnection.close();
    } catch (SQLException e) {
      System.out.println("Connection closing failed");
    }
  }
}
