package jdbcproject;


import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class SqlDisplay {
  public void displayResult(ResultSet queriedResult) {
    ResultSetMetaData MetaData;
    try {

      MetaData = queriedResult.getMetaData();
      for (int ColoumnIterator = 1; ColoumnIterator <= MetaData
          .getColumnCount(); ColoumnIterator++) {
        System.out.printf("|\t%-20s\t|", MetaData.getColumnName(ColoumnIterator));
      }
      System.out.println();
      while (queriedResult.next()) {
        for (int ColoumnIterator = 1; ColoumnIterator <= MetaData
            .getColumnCount(); ColoumnIterator++)
          System.out.printf("|\t%-20s\t|",
              queriedResult.getString(MetaData.getColumnName(ColoumnIterator)));
        System.out.println();
      }
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }

  public void generateReport(ResultSet queriedResult, String filename) {
    ResultSetMetaData MetaData;
    File reportfile = new File(filename);
    try {
      FileWriter filewriter = new FileWriter(reportfile);
      PrintWriter printwriter = new PrintWriter(filewriter);
      MetaData = queriedResult.getMetaData();

      for (int ColoumnIterator = 1; ColoumnIterator <= MetaData
          .getColumnCount(); ColoumnIterator++) {
        printwriter.printf("|%-30s|", MetaData.getColumnName(ColoumnIterator));
      }
      printwriter.printf("\n");
      while (queriedResult.next()) {
        for (int ColoumnIterator = 1; ColoumnIterator <= MetaData
            .getColumnCount(); ColoumnIterator++) {
          printwriter.printf("|%-30s|",
              queriedResult.getString(MetaData.getColumnName(ColoumnIterator)));
        }
        filewriter.write("\n");
      }
      filewriter.close();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }
}
